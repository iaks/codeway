#include<stdio.h>
#include<stdlib.h>

int ll_menu();
void display();
void insert_first();
void insert_last();
void insert_anypos();
void del_first();
void del_last();
void del_anypos();
void search();

struct node {
		int val;
		struct node *next;
};

struct node *head = NULL;

void main() {
		int choice;
		printf("\n\n *** Simple LinkList Implementation in C *** \n\n");

		do {
				choice = ll_menu();
				switch(choice) {
						case 1 : display();
									break;
						case 2 : insert_first();
									break;
						case 3 : insert_last();
									break;
						case 4 : insert_anypos();
									break;
						case 5 : del_first();
									break;
						case 6 : del_last();
									break;
						case 7 : del_anypos();
									break;
						case 8 : search();
									break;
						case 9 : exit(0);

						default : printf("\n\n Wrong Input \n\n");
									// exit(0);
				}
		} while( choice <= 9 );
}

int ll_menu() {
		int user_input;
		
		printf("\n press 1 > display Link_List");
		printf("\n press 2 > insert at first element of LinkList");
		printf("\n press 3 > insert at last element of Link_List");
		printf("\n press 4 > insert at any position of Link_List");
		printf("\n press 5 > delete first element of Link_List");
		printf("\n press 6 > delete last element of Link_List");
		printf("\n press 7 > delete at any position of Link_List");
		printf("\n press 8 > search element in Link_List");
		printf("\n press 9 > exit\n\n");
		
		scanf("%d", &user_input);

	return user_input; 
}

void display() {
		struct node *temp;
		temp = head;

		if (temp == NULL) {
				printf("\nLink-list is Empty.\n");
		}
		else {
				printf("\nLink-List");
				printf("\n------------\n");
				
				while(temp != NULL) {
						
						printf(" %d", temp -> val);
						printf("\n___\n");
						
						temp = temp -> next;
				}
		}
}

void insert_first() {
		int value;
		printf("\n Enter value for the first node");
		scanf("%d", &value);

		struct node *new_node = (struct node*) malloc(sizeof(struct node));
		new_node -> val = value;
		new_node -> next = head;
		head = new_node;
		printf("\n%d inserted at first node\n", new_node -> val);
}

void insert_last() {
		int data;
		printf("\n Enter value for the last node");
		scanf("%d", &data);

		struct node *lastnode, *temp;

		lastnode = (struct node*) malloc(sizeof(struct node));
		lastnode -> val = data;
		lastnode -> next = NULL;
		
		if (head == NULL) {
				head = lastnode;
				printf("\n%d Only node inserted\n", lastnode -> val);
		}
		else {
				temp = head;
				while (temp -> next != NULL) {
						temp = temp -> next;
				}
				temp -> next = lastnode;
				printf("\n%d inserted at Last node\n", lastnode -> val);
		}
}

void insert_anypos() {
	int pos = 0, value = 0, init = 1;
	struct node *point, *nodeanypos;

	if(head == NULL) {
		printf("\nLink-list is empty. Insert at least one node");
		return;
	}
	else {
		nodeanypos = (struct node *) malloc(sizeof(struct node));

		printf("\nEnter value of the new node :");
		scanf("%d", &value);
		nodeanypos -> val = value;
		nodeanypos -> next = NULL;

		printf("\nEnter the position where you want to insert: ");
		scanf("%d", &pos);
		if(pos == 1) {
			nodeanypos -> next = head;
			head = nodeanypos;
		}
		else {
			point = head;
			while(init != (pos - 1)) {
				point = point -> next;
				init++;
			}
			nodeanypos -> next = point -> next;
			point -> next = nodeanypos;

			printf("\n%d node inserted at %d", nodeanypos -> val, pos);
		}
	}
}

void del_first() {
	struct node *temp;
	
	if(head == NULL) {
		printf("\nDeletion not possible. Link_List is already empty.\n");
	}
	else {
		temp = head;
		if(head -> next == NULL) {
			printf("\n%d deleted from Only node remaining.\n", head -> val);
			head = NULL;
			free(temp);
		}
		else {
			printf("\n%d Deleted from first node.\n", head -> val);
			head = head -> next;
			free(temp);
		}
	}
}

void del_last() {
	struct node *temp, *prevTemp;
	if(head == NULL) {
		printf("\nDeletion not possible. Link_List is already empty.\n");
	}
	else {
		temp = head;
		while(temp -> next != NULL) {
			prevTemp = temp;
			temp = temp -> next;
		}
		printf("\n%d value deleted from last node\n", temp -> val);
		prevTemp -> next = NULL;
		free(temp);
	}
}

void del_anypos() {
	struct node *prev, *curr;
	int del_loc, count = 1;
	if(head == NULL) {
		printf("\nDeletion not possible. Link_List is already empty.\n");
	}
	else {
		printf("\nPlease enter the location to delete the node: ");
		scanf("%d", &del_loc);

		curr = head;
		if(del_loc == 1) {
			head = head -> next;
			printf("\n%d Deleted from location %d", curr -> val, del_loc);
			free(curr);
		}
		else {
			for( ; count < del_loc; count++) {
				prev = curr;
				curr = curr -> next;
			}
			prev -> next = curr -> next;
			printf("\n%d deleted from the node %d\n", curr -> val, del_loc);
			free(curr);
		}
	}
} 

void search() {
	if(head == NULL) {
		printf("\nSearch not possible. Link_List is already empty.\n");
	}
	else {
		int search_val;
		printf("\nPlease enter the value to search in the Link-list: ");
		scanf("%d", &search_val);

		struct node *temp;
		temp = head;
		
		int flag = 0;
		int location = 0;

		while(temp != NULL) {
			if(temp -> val == search_val) {
				printf("\nItem found at location %d", (location + 1));
				flag = 1;
			}
			++location;
			temp = temp -> next;
		}

		if(flag == 0) {
			printf("\nItem not found");
		}
	}
}
